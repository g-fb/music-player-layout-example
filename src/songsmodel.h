#ifndef SONGSMODEL_H
#define SONGSMODEL_H

#include <QAbstractTableModel>
#include "song.h"

class SongsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit SongsModel(QObject *parent = nullptr);

    enum Roles {
        ArtistRole = Qt::UserRole,
        TitleRole,
        AlbumRole,
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

public slots:
    Song *song(int index) const;

private:
    QMap<int, QVariant> m_groups;
    QMap<int, Song*> m_songs;
};

#endif // SONGSMODEL_H
