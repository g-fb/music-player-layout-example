#include "song.h"

Song::Song(QObject *parent) : QObject(parent)
{

}

QString Song::artist() const
{
    return m_artist;
}

void Song::setArtist(const QString &artist)
{
    m_artist = artist;
}

QString Song::title() const
{
    return m_title;
}

void Song::setTitle(const QString &title)
{
    m_title = title;
}

QString Song::album() const
{
    return m_album;
}

void Song::setAlbum(const QString &album)
{
    m_album = album;
}

QString Song::disc() const
{
    return m_disc;
}

void Song::setDisc(const QString &disc)
{
    m_disc = disc;
}

QString Song::year() const
{
    return m_year;
}

void Song::setYear(const QString &year)
{
    m_year = year;
}

QString Song::trackNumber() const
{
    return m_trackNumber;
}

void Song::setTrackNumber(const QString &trackNumber)
{
    m_trackNumber = trackNumber;
}

QString Song::length() const
{
    return m_length;
}

void Song::setLength(const QString &length)
{
    m_length = length;
}
