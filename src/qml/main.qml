import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQml 2.12

ApplicationWindow {
    id: window
    property var groups: {"": ""}

    visible: true
    width: 1200
    height: 720
    title: "Tomomi"

    ColumnLayout {
        anchors.fill: parent
        Repeater {
            id: groupRepeater
            model: ["Best Scandal", "Queens Are Trumps"]
            delegate: RowLayout {
                id: group

                Layout.alignment: Qt.AlignTop
                Layout.fillHeight: true
                Layout.fillWidth: true

//                Component.onCompleted: console.log()

                ColumnLayout {
                    Label {
                        text: modelData
                    }

                    Rectangle {
                        width: 300
                        height: 200
                        color: "red"
                    }
                }
                ColumnLayout {
                    id: songs
                    property var m: model
                    Layout.alignment: Qt.AlignTop
                    Layout.fillWidth: true

                    Label {}
                    Repeater {
                        model: songsModel
                        Component.onCompleted: {
                            songsModel.setFilterFixedString(modelData)
                            console.log(modelData)
                        }
                        delegate: SongRow {
                            artist: model.artist
                            title: model.title
                            album: model.album
                        }
                    }
                }
            }
        }
    }
}
