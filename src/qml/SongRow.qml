import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

RowLayout {
    id: root

    property string artist
    property string title
    property string album
    property int firstColWidth: 200
    property int secondColWidth: 300
    property int thirdColWidth: 500

    Label {
        text: artist
        elide: "ElideRight"
        Layout.minimumWidth: firstColWidth
        Layout.maximumWidth: firstColWidth
    }
    Label {
        text: title
        elide: "ElideRight"
        Layout.minimumWidth: secondColWidth
        Layout.maximumWidth: secondColWidth
    }
    Label {
        text: album
        elide: "ElideRight"
        Layout.minimumWidth: thirdColWidth
        Layout.maximumWidth: thirdColWidth
    }
}
