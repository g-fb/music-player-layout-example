#include "songsmodel.h"

#include <QDebug>

SongsModel::SongsModel(QObject *parent)
    : QAbstractTableModel(parent)
{
//    QMap<QString, QVariant> group1;
//    QMap<QString, QVariant> songsG1;

//    QMap<QString, QVariant> song1;
//    song1.insert("artist", "Scandal");
//    song1.insert("title", "Scandal Baby");
//    song1.insert("album", "Best Scandal");
//    songsG1.insert("0", QVariant(song1));
//    QMap<QString, QVariant> song12;
//    song12.insert("artist", "Scandal");
//    song12.insert("title", "Doll");
//    song12.insert("album", "Best Scandal");
//    songsG1.insert("1", QVariant(song12));

//    group1.insert("Songs", QVariant(songsG1));
//    group1.insert("Album", QVariant("Best Scandal"));
//    m_groups.insert(0, QVariant(group1));

//    QMap<QString, QVariant> group2;
//    QMap<QString, QVariant> songsG2;

//    QMap<QString, QVariant> song2;
//    song2.insert("artist", "Scandal");
//    song2.insert("title", "Koe");
//    song2.insert("album", "Queens Are Trumps");
//    songsG2.insert("0", QVariant(song2));

//    group2.insert("Songs", QVariant(songsG2));
//    group2.insert("Album", QVariant("Queens Are Trumps"));
//    m_groups.insert(1, QVariant(group2));

    auto song11 = new Song();
    song11->setArtist("Scandal");
    song11->setTitle("Scandal Baby");
    song11->setAlbum("Best Scandal");
    song11->setYear("2010");
    song11->setTrackNumber("1");
    song11->setDisc("1");
    song11->setLength("5:04");
    m_songs.insert(0, song11);

    auto song12 = new Song();
    song12->setArtist("Scandal");
    song12->setTitle("Doll");
    song12->setAlbum("Best Scandal");
    song12->setYear("2010");
    song12->setTrackNumber("2");
    song12->setDisc("1");
    song12->setLength("3:24");
    m_songs.insert(1, song12);

    auto song21 = new Song();
    song21->setArtist("Scandal");
    song21->setTitle("Koe");
    song21->setAlbum("Queens Are Trumps");
    song21->setYear("2012");
    song21->setTrackNumber("07");
    song21->setDisc("1");
    song21->setLength("4:50");
    m_songs.insert(2, song21);
}

int SongsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_songs.count();
}

int SongsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 4;
}

QVariant SongsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

//    auto group = m_groups[index.row()].toMap();

//    switch (role) {
////    case ArtistRole:
////        return QVariant(song->artist());
////    case TitleRole:
////        return QVariant(song->title());
//    case AlbumRole:
//        return QVariant(group["Album"]);
//    case SongsCountRole:
//        return QVariant(group["Songs"].toMap().count());
//    case SongsRole:
//        return QVariant(group["Songs"].toMap());
//    default:
//        return QVariant();
//    }
    auto song = m_songs[index.row()];

    switch (role) {
    case ArtistRole:
        return QVariant(song->artist());
    case TitleRole:
        return QVariant(song->title());
    case AlbumRole:
        return QVariant(song->album());
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> SongsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ArtistRole] = "artist";
    roles[TitleRole] = "title";
    roles[AlbumRole] = "album";
    return roles;
}

Song *SongsModel::song(int index) const
{
    return m_songs[index];
}
