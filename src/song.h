#ifndef SONG_H
#define SONG_H

#include <QObject>

class Song : public QObject
{
    Q_OBJECT
public:
    explicit Song(QObject *parent = nullptr);

public slots:
    QString artist() const;
    void setArtist(const QString &artist);

    QString title() const;
    void setTitle(const QString &title);

    QString album() const;
    void setAlbum(const QString &album);

    QString disc() const;
    void setDisc(const QString &disc);

    QString year() const;
    void setYear(const QString &year);

    QString trackNumber() const;
    void setTrackNumber(const QString &trackNumber);

    QString length() const;
    void setLength(const QString &length);

signals:

public slots:

private:
    QString m_artist;
    QString m_title;
    QString m_album;
    QString m_disc;
    QString m_year;
    QString m_trackNumber;
    QString m_length;
};

#endif // SONG_H
